#ifndef FFT_SEQ_H 
#define FFT_SEQ_H 

#include <complex.h>

#include <math.h>

#define PI atan2(1, 1) * 4

void fft(complex buf[], int n);

#endif /* FFT_SEQ_H */
