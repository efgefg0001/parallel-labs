
#include <stdio.h>

#include "fft_seq.h"

#define BUF_SIZE 500000
#define EPS 1e-10

void read(const char *path, complex buf[], int *count) {
    FILE *fin = fopen(path, "r");
    if (fin == NULL)
        printf("No such file: %s", path);
    int i = 0;
    int ret = fscanf(fin, "%d", count);
    float read_value;
    ret = fscanf(fin, "%f", &read_value);
    while (ret != EOF) {
        buf[i] = read_value;
        ++i;
        ret = fscanf(fin, "%f", &read_value);
    }
    fclose(fin);
}

void show(const char *path, complex buf[], int count) {
    FILE *fout = fopen(path, "w");
    if (fout == NULL)
        printf("Could not create file: %s", path);
    fprintf(fout, "%d\n", count);
    for (int i = 0; i < count; ++i) {
        if (fabs(cimag(buf[i])) < EPS)
            fprintf(fout, "%g ", creal(buf[i]));
        else
            fprintf(fout, "%g+%g*i ", creal(buf[i]), cimag(buf[i]));
    }
    fprintf(fout, "\n");
    fclose(fout);
}


int main(int argc, const char *argv[]) {
    complex buf[BUF_SIZE]; //{1, 1, 1, 1, 0, 0, 0, 0};

    int count;
    const char *input_file_name = argv[1];
    const char *output_file_name = argv[2];

    read(input_file_name, buf, &count);

    fft(buf, count);

    show(output_file_name, buf, count);
    return 0;
}
