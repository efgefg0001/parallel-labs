#!/usr/bin/env python2

import json
import sys
import math


def read_config(name):
    with open(name) as fin:
        return json.load(fin)


class ConfigProps:
    OUT_FILE = "outFile"
    X_MIN = "xMin"
    COUNT = "count"
    STEP = "step"


def generate(func, x_min, count, out_file, step = 0.01):
    with open(out_file, mode="wt") as fout:
        fout.write(str(count) + "\n")

        for i in range(count):
            value = func(x_min + i * step)
            fout.write("%s " % (value,))

        fout.write("\n")



if __name__ == "__main__":
    config_name = sys.argv[1]
    print(config_name)
    config = read_config(config_name)

    func = lambda x : x #math.sin(x)
    x_min = config[ConfigProps.X_MIN]
    count = config[ConfigProps.COUNT]
    count = count if count % 2 == 0 else count + 1
    out_file = config[ConfigProps.OUT_FILE]
    step = config[ConfigProps.STEP]

    generate(func, x_min, count, out_file, step)

    print("end")
